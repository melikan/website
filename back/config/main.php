<?php
// This is the main configuration

// It is recommended to only use the constructor for the main config
// for example you could serve the clients with homepage if they goes to main URL
// the main URL looks like this http://site_name/

// You could, but it is not recommended to make this file has a sub URL
// the URL if you make sub out of this route would be http://site_name/main/sub
class main{
    function __construct(){
        $this->b=new basic();
        $this->b->setup($this)->serve_html_page(index.html);
    }
}
?>