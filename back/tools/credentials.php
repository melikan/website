<?php
namespace back\tools;

class credentials{
    // ======================================================================
    // Starting http session
    function sess_start(){
        session_start();
        return $this;
    }
    //=======================================================================
    // Setup the session and its value
    function sess_setup($session_arr){
        
        /* Example
        session_name=>session_value

        $session_arr=[
            'name'=>'john doe'
        ]

        */

        $key=array_keys($session_arr)[0];
        $value=$session_arr[$key];
        if($value==''){
            return false;
        }else{
            return $_SESSION[$key]=$value;
        } 
    }
    //=======================================================================
    //=======================================================================
    function sess_req($session_name, $no_exist_redirect=''){
        $SESSION=$_SESSION[$session_name];
        if($no_exist_redirect!=''){
            if($SESSION==''){
                header("Location: ".$no_exist_redirect);
                die("No valid session");
            }
            return $SESSION;
        }else{
            if($SESSION==''){
                echo "No session with that name setup";
            }
            return $SESSION;
        }
    }
    //=========================================================================
    //=========================================================================
    // Setup http cookie
    function cookie_setup($arr_cookie){
        /* Example
        
        $arr_cookie=[
            'name'=>'test',
            'value'=>'well this is cookies', // There is a limit of 1099 char in cookie's length
            'expire'=>'5Y 2D',// n_Year, n_Month, n_Day, n_Hour, n_Minute, n_Second
            'path'=>'/',
            'domain'=>'',
            'secure'=>'false',
            'httponly'=>'false'
        ];
        
        */

        // To setup cookies
        // name, value, expires, path, domain, secure, httponly        
        $c_name; 
        $c_value; 
        $c_expr; 
        $c_path; 
        $c_domain; 
        $c_secure; 
        $c_httponly;

        $critical_err=array();
        $err_pos=0;
        
        // Cookie name is a must
        if($arr_cookie['name'] == null){
            $critical_err[$err_pos]=1;
            $err_pos+=1;
            return false;
        }
        $c_name=$arr_cookie['name'];

        // Cookie value is a must
        if($arr_cookie['value'] == null){
            $critical_err[$err_pos]=1;
            $err_pos+=1;
            return false;
        }
        $c_value=$arr_cookie['value'];
        
        // If no expire is set then expire in end of session
        if($arr_cookie['expire'] == null){
            $c_expr=0;
        }else{
            // Y, M, D, H, MI, S
            // Years, Months, Days, Hours, Minutes, Seconds 
            
            $temp=strtolower($arr_cookie['expire']);
            $final='';
            // Somehow trim function is not working here
            // Replace with this
            $whitespace=0;
            for($x=0;$x<strlen($temp);$x++){
                if(ctype_space($temp[$x])){
                    $whitespace+=1;
                    for($i=0;$i<strlen($temp)-$x;$i++){
                        $temp[$x+$i]=$temp[$x+$i+1];
                    }

                }
            }
            for($i=0;$i<strlen($temp)-$whitespace;$i++){
                $final[$i]=$temp[$i];    
            }
            //=============================================
            
            // Set the variable to null first
            $Y=''; 
            $M=''; 
            $D=''; 
            $H=''; 
            $MI=''; 
            $S='';

            // Check the length of syntax of cookie's expire
            $length=strlen($final);
            
            for($y=0;$y<$length;$y++){
                // If the letter after number is y (stand for year)
                if($final[$y]=='y'){
                    $n=$y+1;
                    // Keep moving until found another letter or the syntax is exhausted to read
                    for($i=$y;$i>0;$i--){
                        if($final[$i-1] == 'm' || $final[$i-1] == 'd' || $final[$i-1] == 'h' || $final[$i-1] == 'i' || $final[$i-1] == 's' ){
                            break;
                        }else{
                            if($n>0){
                                $n-=1;
                            }else{
                                $n=0;
                            }
                            $Y[$n]=$final[$i-1];
                            
                        }
                        
                    }
                }
                // If the letter after number is m (stands for month)
                // prevent i because mi is used for minute
                if($final[$y]=='m' && $final[$y+1]!='i'){
                    $n=$y+1;
                    // Keep moving until found another letter or the syntax is exhausted to read
                    for($i=$y;$i>0;$i--){
                        if($final[$i-1] == 'y' || $final[$i-1] == 'd' || $final[$i-1] == 'h' || $final[$i-1] == 'i' || $final[$i-1] == 's' ){
                            break;
                        }else{
                            if($n>0){
                                $n-=1;
                            }else{
                                $n=0;
                            }
                            $M[$n]=$final[$i-1];
                            
                        }
                        
                    }
                }
                // If the letter after the number is d (stands for day)
                if($final[$y]=='d'){
                    $n=$y+1;
                    // Keep moving until found another letter or the syntax is exhausted to read
                    for($i=$y;$i>0;$i--){
                        if($final[$i-1] == 'm' || $final[$i-1] == 'y' || $final[$i-1] == 'h' || $final[$i-1] == 'i' || $final[$i-1] == 's' ){
                            break;
                        }else{                            
                            if($n>0){
                                $n-=1;
                            }else{
                                $n=0;
                            }
                            $D[$n]=$final[$i-1];
                            
                        }
                        
                    }
                }
                // If the letter after the number is h (stands for hour)
                if($final[$y]=='h'){
                    $n=$y+1;
                    // Keep moving until found another letter or the syntax is exhausted to read
                    for($i=$y;$i>0;$i--){
                        if($final[$i-1] == 'd' || $final[$i-1] == 'm' || $final[$i-1] == 'y' || $final[$i-1] == 'i' || $final[$i-1] == 's' ){
                            break;
                        }else{
                            
                            if($n>0){
                                $n-=1;
                            }else{
                                $n=0;
                            }
                            $H[$n]=$final[$i-1];
                            
                        }
                        
                    }
                }
                // If the letter after the number is am and followed by i (stands for minute)
                if($final[$y]=='i' && $final[$y-1]=='m'){
                    $n=$y+1;
                    // Keep moving until found another letter or the syntax is exhausted to read
                    for($i=$y-1;$i>0;$i--){
                        if($final[$i-1] == 'h' || $final[$i-1] == 'd' || $final[$i-1] == 'm' || $final[$i-1] == 'y' || $final[$i-1] == 's' ){
                            break;
                        }else{                            
                            if($n>0){
                                $n-=1;
                            }else{
                                $n=0;
                            }
                            $MI[$n]=$final[$i-1];
                            
                        }
                        
                    }
                    
                }
                // If the letter after the number is s (stands for second)
                if($final[$y]=='s'){
                    $n=$y+1;
                    // Keep moving until found another letter or the syntax is exhausted to read
                    for($i=$y;$i>0;$i--){
                        if($final[$i-1] == 'i' || $final[$i-1] == 'h' || $final[$i-1] == 'd' || $final[$i-1] == 'm' || $final[$i-1] == 'y' ){
                            break;
                        }else{                      
                            if($n>0){
                                $n-=1;
                            }else{
                                $n=0;
                            }
                            $S[$n]=$final[$i-1];
                            
                        }
                        
                    }
                }
            }
            
            // Because cookie expire is work in seconds convert all to second
            // Conversion to second
            $Y_C_S=0;
            $M_C_S=0;
            $D_C_S=0;
            $H_C_S=0;
            $MI_C_S=0;
            // Converting years to second
            if($Y!=''){
                $Y_C_S=60*60*24*30*12*$Y;
            }
            // Converting months to second
            if($M!=''){
                $M_C_S=60*60*24*30*$M;
            }
            // Converting days to second
            if($D!=''){
                $D_C_S=60*60*24*$D;
            }
            // Converting hours to second
            if($H!=''){
                $H_C_S=60*60*$H;
            }
            // Converting minutes to second
            if($MI!=''){
                $MI_C_S=60*$MI;
            }
            // Converting second to 0 if no second in syntax
            if($S==''){
                $S=0;
            }else{
                $S=$S;
            }

            // Sum the second and get the expire time
            $tot_time=time() + $S + $MI_C_S + $H_C_S + $D_C_S + $M_C_S + $Y_C_S;
            $c_expr=$tot_time;
            
        }
        // Path is not a must, however, if no path is set, set it to /
        if($arr_cookie['path'] == null){
            $c_path='/';
        }else{
            $c_path=$arr_cookie['path'];
        }
        // If no domain is set, set to everywhere
        if($arr_cookie['domain'] == null){
            $c_domain='';
        }else{
            $c_domain=$arr_cookie['domain'];
        }
        // Setup the cookie security
        if($arr_cookie['secure'] == null){
            $c_secure=false;
        }else{
            if($arr_cookie['secure'] == 'false'){
                $c_secure=false;
            }else if($arr_cookie['secure'] == 'true'){
                $c_secure=true;
            }
        }
        // Setup the cookie protocol compatibility
        if($arr_cookie['httponly'] == null){
            $c_httponly=false;
        }else{
            if($arr_cookie['httponly'] == 'false'){
                $c_httponly=false;
            }else if($arr_cookie['httponly'] == 'true'){
                $c_httponly=true;
            }
        }
        
        // If some critical error exist don't build the cookie
        if(count($critical_err) > 0){
            echo 'Error: Failed to create cookie';
            return false;
        }else{
            $s=setcookie($c_name, $c_value, $c_expr, $c_path, $c_domain, $c_secure, $c_httponly);
            if($s){
                return true;
            }else{
                return false;
            }

        }

    }
    //=========================================================================
    //=========================================================================
    function req_cookie($cookie_name, $no_exist_redirect=''){
        $COOKIE=$_COOKIE[$cookie_name];
        if($no_exist_redirect==''){
            if($COOKIE==NULL){
                echo "No cookie with that name setup";
            }
            return $COOKIE;
        }else if($no_exist_redirect!=''){
            if($COOKIE==NULL){
                header("Location: ".$no_exist_redirect);
                die("No cookie with that name");    
            }
            return $COOKIE;
        }
        
    }
    //==========================================================================
}
?>