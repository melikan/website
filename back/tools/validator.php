<?php
namespace back\tools;
class validator{
    
//====================================================================================================================
    function basic_validator($arr_data){
        // The separator between type (if the data should be valid in more than one type) is (.)
        // List of type:
        // 	* Email
        // 	* Username
        //	* Password
        //	* Date
        //	* Alphaonly (Alphabet only)
        //	* Numonly (Numeric only)
        
        // type=>datum,
        // if there are more than one type, 
        // becareful not to make it crash each other 
        // type.type2=>datum
        
        // The valid form of array for this method is like sql return data
        /*
        $arr_data=[
            ['username'=>'johndoe'],
            ['email'=>'johndoe@dom.top'],
            ['password'=>'@JohnDoe123'],
            ['Date'=>'1/5/2022'],
            ['alphaonly'=>'JohnDoe'],
            ['numonly'=>'123567'],
            ['/(name)/'=>'name']
        ];
        */

        // Check the number_key
        $n_keys = count(array_keys($arr_data));
        $valid  = array();
        for($x=0;$x<$n_keys;$x++){
            $n_key          = $arr_data[$x];
            $type_s         = array_keys($n_key)[0]; // What datum should be
            $v_datum        = $n_key[$type_s]; // Datum to validate
            $multi_type     = explode('.', $type_s); // In case what datum should be contain more than one type
            $multi_type_n   = count($multi_type); // Count the multi type
            $valid[$x]      = 1;
               
            for($y=0;$y<$multi_type_n;$y++){
                $type=$multi_type[$y]; // Get the single type to validate with the datum
                // Why multiple strtolower, 
                // because if the type is not found it is predicted to be custom regex 
                if(strtolower($type)=='username'){
                    // Validating username
                    // It can have alphabet characters
                    // It can have numeric characters
                    // It can have underscore
                    // it should be 8 to 30 characters
                    // it should not have special characters
                    $pattern1 = '/[\w]{8,30}/';
                    $pattern2 = '/[\W]{1,}/';
                    
                    $valid[$x] = $valid[$x] & preg_match($pattern1, $v_datum)==1 && preg_match($pattern2, $v_datum)==0 ? 1 : 0; 

                }else if(strtolower($type)=='password'){
                    // Validating password
                    // Should at least 8 characters, maximum 30 characters
                    // At least one uppercase character
                    // At least one special characters
                    // At least one digit
                    $pattern1 = '/\w{8,30}/';
                    $pattern2 = '/[A-Z]{1,}/';
                    $pattern3 = '/\W{1,}/';
                    $pattern4 = '/\d{1,}/';

                    $valid[$x] = $valid[$x] & preg_match($pattern1, $v_datum)==1 && preg_match($pattern2, $v_datum)==1 && preg_match($pattern3, $v_datum)==1 && preg_match($pattern4, $v_datum)==1 ? 1 : 0;
                    
                }else if(strtolower($type)=='email'){
                    // Validating Email
                    // RFC 5322
                    $pattern = "/(?:[a-z0-9!#$%&'*+\\/=?^_`{|}~-]+(?:\\.[a-z0-9!#$%&'*+\\/=?^_`{|}~-]+)*|\"(?:[\\x01-\\x08\\x0b\\x0c\\x0e-\\x1f\\x21\\x23-\\x5b\\x5d-\\x7f]|\\\\[\\x01-\\x09\\x0b\\x0c\\x0e-\\x7f])*\")@(?:(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?|\\[(?:(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\\.){3}(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?|[a-z0-9-]*[a-z0-9]:(?:[\\x01-\\x08\\x0b\\x0c\\x0e-\\x1f\\x21-\\x5a\\x53-\\x7f]|\\\\[\\x01-\\x09\\x0b\\x0c\\x0e-\\x7f])+)\\])/";
                    
                    $valid[$x] = $valid[$x] & preg_match($pattern, $v_datum) ? 1 : 0; 
                    
                }else if(strtolower($type)=='date'){
                    // Validating date
                    // The first form dd/mm/yyyy
                    // The second form yyyy/mm/dd
                    $pattern  = '/([0-9]{2})(\/{1})([0-9]{2})(\/{1})([0-9]{4})/'; // Type one dd/mm/yyyy
                    $pattern2 = '/([0-9]{4})(\/{1})([0-9]{2})(\/{1})([0-9]{2})/'; // Type two yyyy/mm/dd
                    
                    $valid[$x] = $valid[$x] & preg_match($pattern, $v_datum) || preg_match($pattern2, $v_datum) ? 1 : 0;
                    
                }else if(strtolower($type)=='alphaonly'){
                    // validating alphabet only
                    $pattern    = '/[a-zA-Z]{1,}/'; // At least one appearance of alphabet
                    $pattern2   = '/[0-9]/'; // Existence of number
                    $pattern3   = '/[\W]/'; // Existence of special character
                    $pattern4   = '/\_/'; // Existence of underscore
                    
                    $valid[$x]  = $valid[$x] & preg_match($pattern, $v_datum)==1 && preg_match($pattern2, $v_datum)==0 && preg_match($pattern3, $v_datum)==0 && preg_match($pattern4, $v_datum)==0 ? 1 : 0;  

                }else if(strtolower($type)=='numonly'){
                    // Validating numeric only
                    $pattern    = '/[0-9]{1,}/'; // At least one number exists
                    $pattern2   = '/[a-zA-Z]/'; // Existence of word
                    $pattern3   = '/[\W]/'; // Existence of special character
                    $pattern4   = '/\_/'; // Existence of underscore
                    
                    $valid[$x] = $valid[$x] & preg_match($pattern, $v_datum)==1 && preg_match($pattern2, $v_datum)==0 && preg_match($pattern3, $v_datum)==0 && preg_match($pattern4, $v_datum)==0 ? 1 : 0;
                     

                }else{
                    // If the type is unknown then it is a custom type
                    $valid[$x] = $valid[$x] & preg_match($type, $v_datum) ? 1 : 0;
                }
            }
        }
        // Check which data is not valid
        // return false if there is not valid datum
        // return true if all data valid
        
        for($i=0;$i<count($valid);$i++){
            if($valid[$i]==0){
                return false;
            }
            if($i==count($valid)-1){
                return true;
            }
        }
    }
    //=========================================================================================================
}
?>