<?php
namespace back\tools;

class db{
    private $conn;
    private $data_holder;
    private $read_data;
    
    //================================================================================================
    function __construct($db_connection_obj){
        $this->conn=$db_connection_obj;
    }
    //================================================================================================
    
    //================================================================================================
    function fetchHelper(){
        //Need fix for multi way of db connection
        return $this->read_data->fetchAll($this->conn::FETCH_ASSOC);
    }
    //================================================================================================

    //================================================================================================
    function create($data){
        /* Example
        
        $data=[
            'db'=>'db_name',
            'table'=>[
                'table_name'=>[
                    'field'=>['type', 'null', 'default', 'extra', 'key']
                ]
            ],
            'user'=>['username', 'password', 'host', [authority], 'db_name.table_name']
        ];

        Other Example
        $data=[
            'db'=>'db_name',
            'table'=>[
                'table_name'=>[
                    'id'=>['int', 'not', '', 'auto_increment', 'primary_key'],
                    'country'=>['varchar(25)', 'not'],
                    'house_no'=>['int', 'not'],
                    'user_id'=>['int', 'not', '', '', 'foreign_key','other_table(other_table_column)']
                ]
            ],
            'user'=>['johndoe', 'johndoepass123', 'localhost', ['create', 'drop'], 'world.*']
        ];

        */
        
        $creation_names     = array_keys($data);
        $creation_names_n   = count($creation_names);
        $success=$fail      = 0;

        for($a=0;$a<$creation_names_n;$a++){
            $creation_name=$creation_names[$a];
            
            // Process one by one three possibility of creation_names
            if($creation_name=='db'){
                // Valid query
                // CREATE DATABASE database_name

                $dbname=$data[$creation_name];
                if($dbname==''){
                    return false;
                }else{
                    $syntax="CREATE DATABASE ".$dbname;
                }
                $q=$this->conn->query($syntax);
                if($q){
                    $success+=1;
                }else{
                    $fail+=1;
                }

            }else if($creation_name=='table'){
                // Valid query
                // CREATE TABLE table_name(column_name data_type null_key default extra, 
                // ..., column_nameN data_type null_key default extra, primary key(column_name), 
                // foreign key(column_name) references other_table_name(other_column_name));
                
                $place          = $data[$creation_name];
                $table_names    = array_keys($place);
                $table_names_n  = count($table_names);
                $syntax_a       = "CREATE TABLE ";
                $syntax_b;
                $syntax_c       = ',';
                $syntax_d;
                $syntax_holder  = ' ';
                for($x=0;$x<$table_names_n;$x++){
                    $table_name     = $table_names[$x];
                    $syntax_a       = $syntax_a.$table_name;
                    $column_names   = array_keys($place[$table_name]);
                    $column_names_n = count($column_names);
                    $y=$z           = 0;
                    for(;$y<$column_names_n;$y++){
                        $column_name    = $column_names[$y];
                        $z+=1;
                        $items          = $place[$table_name][$column_name];
                        $data_type      = $items[0];
                        $null_key       = $items[1];
                        $default        = $items[2];
                        $extra          = $items[3];
                        $keying         = $items[4];

                        // column_name and data_type is a must, prevent array without it
                        if($column_name=='' && $data_type==''){
                            return false;
                            break;
                        }

                        // Setup null key
                        if($null_key=='not'){
                            $null_key='not null';
                        }else if($null_key=='' || $null_key==null){
                            $null_key='';
                        }

                        // Prepare comma
                        if($z>=$column_names_n){
                            $comma='';
                        }else{
                            $comma=', '; 
                        }

                        // Setup constrain keys
                        if($keying=='primary_key'){
                            $keying=' primary key('.$column_name.')';
                        }else if($keying=='foreign_key'){
                            $keying=' foreign key('.$column_name.')'.' references'.' '.$items[5];
                        }

                        // Setup optional keys
                        $syntax_holder=' '.$null_key.' '.$default.' '.$extra;
                        
                        $syntax_b=$syntax_b.$column_name.' '.$data_type.$syntax_holder.$comma;
                        
                        // If constraint keys exist add one more place per key
                        if($keying!=''){
                            
                            $syntax_c=$syntax_c.$keying.$comma;
                        }
                    }
                    $syntax_d=$syntax_a.'('.$syntax_b.$syntax_c.')';
                    $q1=$this->conn->query($syntax_d);
                    if($q1){
                        $success+=1;
                    }else{
                        $fail+=1;
                    }
                }
            }else if($creation_name=='user'){
                
                // Valid query
                // CREATE USER 'user_name'@'host' IDENTIFIED BY 'password'
                // GRANT {privileges} ON db.table 'user_name'@'host'

                $place=$data[$creation_name];
                
                // First action creating user
                //===========================================================
                // Check if name exist
                if($place[0]==''){
                    return false;
                }else{
                    $name=$place[0];
                }

                // Check if password exist
                $password=$place[1];
                if($password==''){
                    $syntax_f="";
                }else{
                    $syntax_f=" IDENTIFIED BY ".'"'.$password.'"';
                }

                // Check if host exist
                if($place[2]==''){
                    $place[2]='localhost';
                }
                $host=$place[2];
                
                $syntax_e="CREATE USER ".'"'.$name.'"'.'@"'.$host.'"'.$syntax_f;
                //===========================================================
                $q2=$this->conn->query($syntax_e);
                if($q2){
                    $success+=1;
                }else{
                    $fail+=1;
                }
                // Second action granting privileges
                //===========================================================
                $privileges     = $place[3];
                $privileges_n   = count($privileges);
                $syntax_g;

                if($privileges_n==1){
                    $syntax_g=$privileges;
                }else if($privileges_n>1){
                    $x=$i=0;
                    for(;$x<$privileges_n;$x++){
                        $i+=1;
                        if($i>=$privileges_n){
                            $comma='';
                        }else{
                            $comma=', ';
                        }
                        $syntax_g=$syntax_g.$privileges[$x].$comma;
                    }
                }
                $privilege_location = $place[4];
                $syntax_h           = " GRANT ".$syntax_g." ON ".$privilege_location.' TO "'.$name.'"@'.'"'.$host.'"';
                $q3                 = $this->conn->query($syntax_h);
                if($q3){
                    $success+=1;
                }else{
                    $fail+=1;
                }
                
                //===========================================================
            }
        }
        echo "Creation done:".$success." success & ".$fail." failed";
    }
    //================================================================================================
    
    //================================================================================================
    function read($arr_query, $flag='OR'){
        /* Example
        
        // F : Form

        // F1
        $arr_query=[
            'dbname'=>'db_name',
            'table'=>[
                'table_name'=>[
                    'select'=>["field1", ..., "fieldN"],
                    'where'=>[
                        'fieldX'=>['!=','datum'],
                        'fieldY'=>'datum'
                    ]
                ]
            ]
        ];

        // F2
        $arr_query=[
            'table_name'=>[
                'select'=>['field1', ..., 'fieldN'],
                'where'=>[
                    'fieldX'=>['!=','datum'],
                    'fieldY'=>'datum'
                ]
            ]
        ];

        // F3
        $arr_query=[
            'table_name'=>[
                '*',
                'where'=>[
                    'fieldX'=>['!=','datum'],
                    'fieldY'=>'datum'
                ]
            ]
        ];

        // F4 (Select all)
        $arr_query=[
            'table_name'=>'*'
        ];

        // F5
        $arr_query=[
            'table_name'=>['field1', ..., 'fieldN']
        ];

        */
        
        // Valid query
        // Select column_name, ..., column_nameN from db.table_name 
        // where column_name(operator)datum (flag) column_nameN(operator) datum;
        
        // Valid query
        // Select * from table_name;
         $first_keys    = array_keys($arr_query);
         $first_keys_n  = count($first_keys);
         if($first_keys_n==2){
            $syntax_a   = "SELECT ";
            $syntax_b;
            $syntax_c   = " FROM ";
            $syntax_d   = " WHERE ";
            for($x=0;$x<2;$x++){
                $first_key=$first_keys[$x];
                if($first_key=='dbname'){
                    $dbname=$arr_query[$first_key];

                }else if($first_key=='table'){
                    $table_names    = array_keys($arr_query[$first_key]);
                    $table_name     = $table_names[0];
                }
                
            }
            $syntax_c       = $syntax_c.$dbname.'.'.$table_name;
            $holder         = $arr_query['table'][$table_name];
            $third_keys     = array_keys($holder);
            $third_keys_n   = count($third_keys);
            for($x=0;$x<$third_keys_n;$x++){
                $third_key=$third_keys[$x];
                if($third_key=='select'){
                    $arr_select     = $holder[$third_key];
                    $arr_select_n   = count($arr_select);
                    $i=$c=0;
                    for(;$i<$arr_select_n;$i++){
                        $c+=1;
                        $select_column=$arr_select[$i];
                        if($c>=$arr_select_n){
                            $comma='';
                        }else{
                            $comma=', ';
                        }
                        $syntax_b=$syntax_b.$select_column.$comma;
                    }
                }else if($third_key=='where'){
                    $arr_where               = $holder[$third_key];
                    $column_names_in_where   = array_keys($arr_where);
                    $column_names_in_where_n = count($column_names_in_where);
                    $x=$c=0;
                    for(;$x<$column_names_in_where_n;$x++){
                        $c+=1;
                        $column_name    = $column_names_in_where[$x];
                        $data_where     = $arr_where[$column_name];
                        $data_where_n   = count($data_where);

                        if($data_where_n==2){
                            $operator    = $data_where[0];
                            $datum_where = $data_where[1];   
                        }else{
                            $operator='=';
                            $datum_where = $data_where;
                        }
                        if($c>=$column_names_in_where_n){
                            $flag='';
                        }else{
                            $flag=' '.strtoupper($flag).' ';
                        }
                        $syntax_d=$syntax_d.$column_name.$operator.'"'.$datum_where.'"'.$flag;    
                        
                    }

                }
            }
            $syntax_a        = $syntax_a.$syntax_b.$syntax_c.$syntax_d;
            $this->read_data = $this->conn->query($syntax_a);
            
            
         }else if($first_keys_n==1){
             $table_name    = $first_keys[0];
             $second_keys   = array_keys($arr_query[$table_name]);
             $second_keys_n = count($second_keys);
             
             if($second_keys_n>1){
                    $syntax_a = "SELECT ";
                    $syntax_d = " FROM ".$table_name;
                    for($x=0;$x<$second_keys_n;$x++){

                        $second_key = $second_keys[$x];
                        $sw         = $arr_query[$table_name][$second_key];
                        $y=$c=0;
                        if($second_key=='select'){
                            $column_names_select    = $sw;
                            $column_names_select_n  = count($column_names_select);
                            
                            for(;$y<$column_names_select_n;$y++){
                                $c+=1;
                                
                                $column_name_select=$column_names_select[$y];
                                if($c>=$column_names_select_n){
                                    $comma='';
                                }else{
                                    $comma=', ';
                                }
                                $syntax_b=$syntax_b.$column_name_select.$comma;
                            }
                        }else if($second_key=='where'){
                            $syntax_d       = $syntax_d." WHERE ";
                            $third_keys     = array_keys($sw);
                            $third_keys_n   = count($third_keys);
                            for(;$y<$third_keys_n;$y++){
                                $c+=1;
                                $column_name    = $third_keys[$y];
                                $data_where     = $sw[$column_name];
                                $data_where_n   = count($data_where);
                                if($data_where_n==2){
                                    $operator   = $data_where[0];
                                    $datum      = $data_where[1];
                                }else{
                                    $operator   = '=';
                                    $datum      = $data_where;
                                }
                                if($c>=$third_keys_n){
                                    $flag='';
                                }else{
                                    $flag=' '.strtoupper($flag).' ';
                                }
                                $syntax_c=$syntax_c.$column_name.$operator.'"'.$datum.'"'.$flag;
                                
                            }
                        }else{
                            $syntax_b       = '';
                            $second_keys    = $arr_query[$table_name];
                            $second_keys_n  = count($second_keys);
                            for(;$y<$second_keys_n;$y++){
                                $c+=1;
                                $second_key_name=$second_keys[$y];
                                if($c>=$second_keys_n){
                                    $comma='';
                                }else{
                                    $comma=', ';
                                }
                                $syntax_b=$syntax_b.$second_key_name.$comma;
                            }
                        }
                        
                    
                    }
                    $syntax_a        = $syntax_a.$syntax_b.$syntax_d.$syntax_c;
                    $this->read_data = $this->conn->query($syntax_a);
                
                                
             }else if($second_keys_n==0){
                 $s                 = $arr_query[$table_name];
                 $syntax_a          = "SELECT * FROM ".$table_name;
                 $this->read_data   = $this->conn->query($syntax_a);
                 
                 
             }
             
        }
        return $this;

    }
    //=================================================================================================

    //=================================================================================================
    function update($data, $flag=''){
        /* Example
        
        $data=[
            "table_name"=>[
                "set"=>[
                    "field1"=>'datum',
                    "fieldN"=>'datum'
                ],
                "where"=>[
                    "fieldX"=>["!=", "datum"],
                    "fieldY"=>"datum"
                ]
            ]
        ];
        
        */

        if($flag==''){
            $flag="AND";
        }
        $flag=" ".$flag." ";
        $success=$fail=0;
        //Valid query
        // UPDATE table_name SET column_name=value, ..., column_nameN=valueN WHERE column_name(operator)value (flag) column_nameN(operator)value
        
        $syntax_a = "UPDATE ";
        $syntax_b = " SET ";
        //$syntax_c; column_name=value, ..., column_nameN=valueN
        $syntax_d = " WHERE ";
        //$syntax_e; column_name(operator)value (flag) column_nameN(operator)value
        //$syntax_f; combination of a, b, c, d, e
        
        $i=$c=0;
        
        // First step find the First key which is table_name
        $table_names    = array_keys($data);
        $table_names_n  = count($table_names);
        for($x=0;$x<$table_names_n;$x++){

            $table_name = $table_names[$x];
            $syntax_a   = $syntax_a.$table_name;
            
            // Second step find the Second key which is SET and WHERE
            $second_keys    = array_keys($data[$table_name]);
            $second_keys_n  = count($second_keys);
            for($y=0;$y<$second_keys_n;$y++){
                $second_key     = $second_keys[$y];
                
                $column_names   = array_keys($data[$table_name][$second_key]);
                $column_names_n = count($column_names);

                // Process the second key
                if(strtolower($second_key)=='set'){
                    
                    for($z=0;$z<$column_names_n;$z++){
                        $i+=1;
                        $column_name= $column_names[$z];
                        $item       = $data[$table_name][$second_key][$column_name];
                        // Setup the need of comma
                        if($i>=$column_names_n){
                            $comma=" ";
                        }else{
                            $comma=", ";
                        }
                        $syntax_c=$syntax_c.$column_name."=".'"'.$item.'"'.$comma;
                    }    
                    
                }else if(strtolower($second_key)=='where'){
                    for($z=0;$z<$column_names_n;$z++){
                        $c+=1;
                        $column_name = $column_names[$z];
                        $items       = $data[$table_name][$second_key][$column_name];
                        $items_n     = count($items);
                        // If there are two items, the first item is operator the second key is value
                        if($items_n==2){
                            $operator=$items[0];
                            $value=$items[1];
                        }else if($items_n==1){
                            $operator="=";
                            $value=$items;
                        }
                        // Setup the need of comma
                        if($c>=$column_names_n){
                            $flag=" ";
                        }
                        $syntax_e=$syntax_e.$column_name.$operator.'"'.$value.'"'.$flag;
                    }
                }

            }
        }
        // Combine the syntax
        $syntax_f=$syntax_a.$syntax_b.$syntax_c.$syntax_d.$syntax_e;
        
        if($this->conn->query($syntax_f)){
            $success+=1;
        }else{
            $fail+=1;
        }
        // Print the success and fail message
        echo "Update done: ".$success." success & ".$fail." failed";
        
        // Return flag in completion
        if($success>0){
            return true;
        }else{
            return false;
        }
    }
    //=================================================================================================

    //================================================================================================= 
    // $flag is logic operator
    // $data is data for delete operation; see pattern of data in documentation
    function delete($data, $flag=''){
        /*Example
        
        $data=[
                "table_name"=>[
                    "field1"=>['!=', 'datum'],
                    "fieldN"=>'datum'
                ]
        ];

        */

        // Set default flag
        if($flag==''){
            $flag="AND";
        }

        // Add space between flag syntax
        $flag=" ".$flag." ";

        $success=$fail=0;

        // First case, deletion using unique key
        // Second case, deletion of all data with one characteristic
        // Third case, deletion of all data with multi characteristic

        // Valid query
        // DELETE FROM table_name WHERE column_name(operator)'value' && column_nameN(operator)'valueN';
        
        $syntax_a = "DELETE FROM ";
        $syntax_b = " WHERE ";
        //$syntax_c; column_name(operator)'value' && column_nameN(operator)'valueN'
        //$syntax_d; combination of a, b, c

        // First key is table_name
        // Second key is column_name
        // Third key (if exist Fourth key) is operator, else it is the item

        // First step find the table_name
        $table_names=array_keys($data);
        for($x=0;$x<count($table_names);$x++){
            $table_name = $table_names[$x];
            $syntax_a   = $syntax_a.$table_name;

            // Second step find the column_name
            $column_names   = array_keys($data[$table_name]);
            $n_column_names = count($column_names);
            $z=$y=0;
            for(;$y<$n_column_names;$y++){
                $column_name = $column_names[$y];
                
                // Third step find the item
                $items = $data[$table_name][$column_name];
                
                // If the items is two it means that it has operator
                $n_items = count($items);
                if($n_items==2){
                    $operator  = $items[0];
                    $item      = $items[1];
                }else if($n_items==1){
                    $operator  = "=";
                    $item      = $items;
                }
                $z+=1;
                if($z>=$n_column_names){
                    $flag=" ";
                }
                $syntax_c=$syntax_c.$column_name.$operator.'"'.$item.'"'.$flag;
                
            }
        }
        $syntax_d=$syntax_a.$syntax_b.$syntax_c;
        
        if($this->conn->query($syntax_d)){
            $success+=1;
        }else{
            $fail+=1;
        }

        // Print the success and fail message
        echo "Deletion done: ".$success." success & ".$fail." failed";
        
        // Return flag in completion
        if($success>0){
            return true;
        }else{
            return false;
        }
    }
    //===================================================================================
    
    //===================================================================================
    function insert($data){
        /*Example

        $data=[
            "table_name"=>[
                "field1"         => 'datum',
                "fieldN"         => 'datum'
            ]
        ];

        */

        // If any failure during the input of data happens, use this
        $success=$fail=0;

        //$table_name;
        //$column_name;
        
        // Valid Query
        // INSERT INTO table_name(column[0], ..., column[n]) VALUES ('value[0]', ..., 'value[n]');

        // $syntax_a : INSERT INTO table_name(column[0], ..., column[n])
        // $syntax_b : VALUES(value[0], ..., value[n])
        // $syntax_c : combination of a, b
        // $comma    : is used to give comma in the syntax
    
        // First step find all the table_name
        $table_names=array_keys($data);
        for($x=0;$x<count($table_names);$x++){
            $table_name     = $table_names[$x];
            $syntax_a       = "INSERT INTO ".$table_name."(";
            $syntax_b       = "VALUES(";

            // Second step find all the column_name
            $column_names   = array_keys($data[$table_name]);
            $y              = 0;
            for(;$y<count($column_names);$y++){
                $i=$y+1;
                
                $column_name    = $column_names[$y];
                $item           = $data[$table_name][$column_name];

                if($i>=count($column_names)){
                    $comma="";
                }else{
                    $comma=", ";
                }
                
                $syntax_a = $syntax_a.$column_name.$comma;
                $syntax_b = $syntax_b.'"'.$item.'"'.$comma;
            }

            $syntax_a = $syntax_a.")";
            $syntax_b = $syntax_b.")";
            $syntax_c = $syntax_a." ".$syntax_b;
            
            // Check and count the failure in insertion
            if($this->conn->query($syntax_c)){
                $success+=1;
            }else{
                $fail+=1;
            }
        
        }
        // Print the success and fail message
        echo "Insertion done: ".$success." success & ".$fail." failed";
        
        //Return flag in completion
        if($success>0){
            return true;
        }else{
            return false;
        }
    }
    //===================================================================================
}

?>